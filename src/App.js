import React, { useEffect, useState } from "react";

import "./App.css";

const formatNumber = (number) => new Intl.NumberFormat("en", { minimumFractionDigits: 2 }).format(number);
const orderData = previous => Object.keys(previous).sort().reduce((obj, key) => {
  obj[key] = previous[key];
  return obj;
}, {});

const filterData = (products, productName) => Object.keys(products).filter(name => {
  return name.toLocaleLowerCase().includes(productName.toLocaleLowerCase());
}).reduce((obj, key) => {
  obj[key] = products[key];
  return obj;
}, {});

const getTotal = products => Object.values(products).reduce((sum, x) => sum + x, 0);
const calculateRevenue = (quantity, unitPrice) => quantity * unitPrice;

function App() {
  const [products, setProducts] = useState({});
  const [loading, setLoading] = useState(true);
  const [productName, setProductName] = useState("");

  const fetchBranch = branchId => {
    fetch(`/api/branch${branchId}.json`)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then(data => {
        setProducts(previous => {
          data.products.forEach(product => {
            const key = product['name'];
            const revenue = calculateRevenue(product['sold'], product['unitPrice']);

            if (previous[key]) {
              previous[key] = previous[key] + revenue;
            }
            else {
              previous[key] = revenue;
            }
          });

          return orderData(previous);
        });
      })
      .catch(error => {
        console.log('error', error);
      })
      .finally(() => {
        setLoading(false);
      })
  }

  useEffect(() => {
    fetchBranch(1);
    fetchBranch(2);
    fetchBranch(3);
  }, []);

  if (loading) {
    return "Loading...";
  }

  const data = filterData(products, productName);

  return (
    <div className="product-list">
      <label>Search Products</label>
      <input value={productName} onChange={(e) => setProductName(e.target.value)} type="text" />
      <table>
        <thead>
          <tr>
            <th>Product</th>
            <th>Revenue</th>
          </tr>
        </thead>
        <tbody>
          {
            Object.keys(data).map(name => {
              return (
                <tr key={name}>
                  <td>{name}</td>
                  <td>{formatNumber(products[name])}</td>
                </tr>
              )
            })
          }
        </tbody>
        <tfoot>
          <tr>
            <td>Total</td>
            <td>{formatNumber(getTotal(data))}</td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
}

export default App;
